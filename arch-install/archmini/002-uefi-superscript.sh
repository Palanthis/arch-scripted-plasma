#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "Ranking mirrors - This may take a few minutes."
echo
echo "Please be patient."
reflector --country 'United States' --age 12 --protocol http --latest 50 --sort rate --save /etc/pacman.d/mirrorlist

echo
echo "Done ranking mirrors!"

cp -r ../post /mnt/root

echo
echo "~/superscript-post-uefi-intel.sh && exit" > /mnt/root/.bashrc

chmod +x /mnt/root/*.sh

pacstrap /mnt base base-devel

genfstab -U -p /mnt >> /mnt/etc/fstab

echo
echo "Ready to arch-chroot and finish setup?"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."
echo
arch-chroot /mnt
