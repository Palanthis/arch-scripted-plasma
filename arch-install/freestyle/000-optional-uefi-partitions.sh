#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/sda"
echo "and /dev/sdb - Do NOT use this script if you need to do"
echo "manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

sfdisk /dev/sda < sda-uefi.sfdisk

read -n 1 -s -r -p "/dev/sda done. Press Enter to continue or Ctrl+C to quit."

sfdisk /dev/sdb < sdb-uefi.sfdisk

partprobe

fdisk -l | grep /dev/sd

echo
echo "Do these partitions look okay?"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.fat -F32 /dev/sda1

mkfs.ext4 -FF /dev/sda2

mkfs.btrfs -f /dev/sdb1

mount /dev/sda2 /mnt

mkdir -p /mnt/boot/efi

mkdir -p /mnt/home

mount /dev/sda1 /mnt/boot/efi

mount /dev/sdb1 /mnt/home

echo
echo "Do these mount points look right?"
echo
mount | grep /dev/sd
