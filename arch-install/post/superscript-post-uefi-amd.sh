#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sed -i 's/^#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen

locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf

export LANG=en_US.UTF-8

ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

hwclock --systohc --utc

echo
echo "What should the hostname be?"

read input

echo $input > /etc/hostname

systemctl enable fstrim.timer

mv /etc/pacman.conf /etc/pacman.bak

cp /root/pacman.conf /etc/pacman.conf

pacman -Sy

echo
echo "Set root password"

passwd

echo
echo "What is your username?"

read input1

useradd -m -g users -G wheel,storage,power -s /bin/bash $input1

passwd $input1

mv /etc/sudoers /etc/sudoers.bak

cp /root/sudoers /etc/sudoers

mv /root/arch-plasma /home/$input1/

chown -R $input1:users /home/$input1/arch-plasma

pacman -S --needed --noconfirm bash-completion efibootmgr grub git
pacman -S --needed --noconfirm networkmanager xdg-user-dirs amd-ucode

efibootmgr

grub-install /dev/sda

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager.service

fallocate -l 2048M /swapfile

chmod 600 /swapfile

mkswap /swapfile

swapon /swapfile

echo "/swapfile none swap defaults 0 0" >> /etc/fstab

swapoff /swapfile

echo

echo "Install Done!"

echo "Taking you back to live environment"

echo "Just run ./finish.sh to complete!"

read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

exit
