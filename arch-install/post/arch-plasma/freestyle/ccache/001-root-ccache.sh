#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo "This script is ONLY for an 8 core CPU, but you can edit"
echo "it for yours!"

# Ensure pacman cache is up-to-date
pacman -Syy

# Install ccache
pacman -S ccache --needed --noconfirm

# Copy over makepkg.conf
cp -f makepkg.conf /etc/makepkg.conf


 
