#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Octopi and utilities
yaourt -S --noconfirm --needed octopi-kde-git
yaourt -S --noconfirm --needed alpm_octopi_utils
yaourt -S --noconfirm --needed octopi-notifier-noknotify
