#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
sudo ./use-all-cores-makepkg.sh

# Network
sudo pacman -S --noconfirm --needed networkmanager
sudo pacman -S --noconfirm --needed network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install trizen
sudo pacman -S --noconfirm --needed trizen

# Install octopi and package-query
trizen -S --noconfirm --needed package-query
#trizen -S --noconfirm --needed octopi
#trizen -S --noconfirm --needed alpm_octopi_utils

# Install pamac
sudo pacman -S --needed --noconfirm pamac-aur
trizen -S --noconfirm pamac-tray-appindicator

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm archiso wget git

# Plasma from Standard Repos
sudo pacman -S --needed --noconfirm aspell-en bluedevil breeze
sudo pacman -S --needed --noconfirm breeze-gtk cdrdao
sudo pacman -S --needed --noconfirm discover dolphin-plugins k3b
sudo pacman -S --needed --noconfirm drkonqi gwenview ffmpegthumbs
sudo pacman -S --needed --noconfirm dvd+rw-tools kde-gtk-config
sudo pacman -S --needed --noconfirm kdebase-meta kdeconnect kfind
sudo pacman -S --needed --noconfirm kdegraphics-thumbnailers kopete
sudo pacman -S --needed --noconfirm kdelibs4support kdeplasma-addons
sudo pacman -S --needed --noconfirm kdeutils-meta kdialog kgamma5
sudo pacman -S --needed --noconfirm khelpcenter kinfocenter konsole
sudo pacman -S --needed --noconfirm kipi-plugins kmenuedit kscreen
sudo pacman -S --needed --noconfirm ksshaskpass ksysguard kwin milou
sudo pacman -S --needed --noconfirm kwayland-integration kwrited
sudo pacman -S --needed --noconfirm kwalletmanager okular oxygen
sudo pacman -S --needed --noconfirm nm-connection-editor user-manager
sudo pacman -S --needed --noconfirm kwallet-pam sweeper sddm-kcm
sudo pacman -S --needed --noconfirm packagekit-qt5 plasma-pa sddm
sudo pacman -S --needed --noconfirm partitionmanager plasma-desktop
sudo pacman -S --needed --noconfirm plasma-nm plasma-sdk spectacle
sudo pacman -S --needed --noconfirm plasma-vault powerdevil
sudo pacman -S --needed --noconfirm plasma-workspace qtcurve-gtk2
sudo pacman -S --needed --noconfirm plasma-workspace-wallpapers
sudo pacman -S --needed --noconfirm qtcurve-kde
sudo pacman -S --needed --noconfirm qtcurve-utils systemsettings
sudo pacman -S --needed --noconfirm kvantum-qt5 kvantum-theme-adapta
sudo pacman -S --needed --noconfirm adapta-gtk-theme 
sudo pacman -S --needed --noconfirm plasma5-applets-weather-widget
sudo pacman -S --needed --noconfirm kdeadmin kdenetwork kdepim

# Enable Display Manager - comment out if keeping current DM
sudo systemctl enable sddm.service

# Sound
sudo pacman -S --noconfirm --needed pulseaudio pulseaudio-alsa pavucontrol
sudo pacman -S --noconfirm --needed alsa-utils alsa-plugins alsa-lib alsa-firmware
sudo pacman -S --noconfirm --needed gst-plugins-good gst-plugins-bad gst-plugins-base
sudo pacman -S --noconfirm --needed gst-plugins-ugly gstreamer kid3-qt

# Fonts from 'normal' repositories
sudo pacman -S --noconfirm --needed noto-fonts noto-fonts-emoji
sudo pacman -S --noconfirm --needed adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --noconfirm --needed chromium geany geany-plugins keepassxc
sudo pacman -S --noconfirm --needed conky conky-manager file-roller evince
sudo pacman -S --noconfirm --needed uget qbittorrent gparted vlc ttf-liberation
sudo pacman -S --noconfirm --needed qterminal neofetch phonon-qt5-vlc
sudo pacman -S --noconfirm --needed cairo-dock cairo-dock-plug-ins
sudo pacman -S --noconfirm --needed asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --noconfirm --needed liboggz youtube-dl reflector tilix
sudo pacman -S --noconfirm --needed rhythmbox firefox audacity ntfs-3g
sudo pacman -S --noconfirm --needed steam steam-native-runtime p7zip
sudo pacman -S --noconfirm --needed grub-customizer os-prober flameshot
sudo pacman -S --noconfirm --needed variety meld gimp inkscape openshot
sudo pacman -S --noconfirm --needed shotwell simplescreenrecorder lolcat
sudo pacman -S --noconfirm --needed redshift plasma5-applets-redshift-control

# System Utilities
sudo pacman -S --noconfirm --needed curl dconf-editor ffmpegthumbnailer
sudo pacman -S --noconfirm --needed dmidecode hardinfo htop mlocate
sudo pacman -S --noconfirm --needed hddtemp lm_sensors lsb-release
sudo pacman -S --noconfirm --needed net-tools numlockx simple-scan
sudo pacman -S --noconfirm --needed scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --noconfirm --needed unclutter putty whois openssh

# Install OpenVPN Components
sudo pacman -S --noconfirm --needed openvpn resolvconf networkmanager-openvpn

# Install Libre Office
sudo pacman -S libreoffice-fresh --noconfirm --needed

# Install VirtualBox and optional components
sudo pacman -S --noconfirm --needed virtualbox
sudo pacman -S --noconfirm --needed virtualbox-host-dkms
sudo pacman -S --noconfirm --needed virtualbox-guest-iso 

# Optional - install xdg-user-dirs and update my home directory.
#This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --noconfirm --needed xdg-user-dirs
xdg-user-dirs-update --force

# Printing Support
sudo pacman -S --noconfirm --needed cups cups-pdf
sudo pacman -S --noconfirm --needed foomatic-db-engine
sudo pacman -S --noconfirm --needed foomatic-db foomatic-db-ppds 
sudo pacman -S --noconfirm --needed foomatic-db-nonfree-ppds 
sudo pacman -S --noconfirm --needed foomatic-db-gutenprint-ppds 
sudo pacman -S --noconfirm --needed ghostscript gsfonts gutenprint
sudo pacman -S --noconfirm --needed gtk3-print-backends
sudo pacman -S --noconfirm --needed libcups
sudo pacman -S --noconfirm --needed hplip
sudo pacman -S --noconfirm --needed system-config-printer
sudo systemctl enable org.cups.cupsd.service

# Network Discovery
sudo pacman -S --noconfirm --needed avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Extras
sudo pacman -S --noconfirm --needed megasync
trizen -S --noconfirm rar
trizen -S --noconfirm gitkraken

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/palanthis.tar.gz -C ~/ --overwrite
sudo tar xzf ../tarballs/templates.tar.gz -C ~/.local/share/ --overwrite

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Copy over SDDM theme
sudo tar xzf ../tarballs/archlinux-sddm-theme.tar.gz -C /usr/share/sddm/themes/ --overwrite

# Copy SDDM config
sudo tar xzf ../tarballs/sddm-conf.tar.gz -C /etc/ --overwrite

# Install wallpapers
[ -d ~/Wallpapers ] || mkdir ~/Wallpapers
tar xzf ../tarballs/wallpapers.tar.gz -C ~/Wallpapers

# Install Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf ../tarballs/conky.tar.gz -C ~/.conky/

# Add screenfetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
