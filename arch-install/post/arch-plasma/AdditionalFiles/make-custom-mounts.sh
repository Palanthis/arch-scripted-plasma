#!/bin/bash
set -e
# make-custom-mounts.sh
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo mkdir -p /mnt/HugeData
sudo mkdir -p /mnt/LinuxTB
sudo mkdir -p /mnt/Passport

sudo chown palanthis *

chmod +rw *
