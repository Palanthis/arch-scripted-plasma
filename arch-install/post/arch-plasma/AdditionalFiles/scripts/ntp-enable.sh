#!/bin/bash
set -e

# Simple script to enable NTP on Arch Linux Systems
# Author: Palanthis - palanthis@protonmail.com
# Date: 03/13/2018

sudo systemctl enable systemd-networkd.service

sudo systemctl start systemd-networkd.service

sudo systemctl enable systemd-timesyncd.service

sudo systemctl start systemd-timesyncd.service

sudo timedatectl set-ntp true

sudo hwclock --systohc --utc

echo "Let's make sure everything looks right!"

timedatectl status
