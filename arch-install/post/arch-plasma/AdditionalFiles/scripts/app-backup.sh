#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
SAVING=/home/$USER
file_version=$(date +%m.%d.%Y)
echo
echo "Backing up package lists!"
echo
sudo [ -d $SAVING/AppBackup ] || sudo mkdir $SAVING/AppBackup
sudo chown $USER:users $SAVING/AppBackup
sudo pacman -Qqen > $SAVING/AppBackup/packages-repository-${file_version}.txt
sudo pacman -Qqem > $SAVING/AppBackup/packages-AUR-${file_version}.txt
echo
echo "ALL DONE!"
echo
echo "Repo List is saved in $SAVING/AppBackup/packages-repository-${file_version}.txt"
echo
echo "AUR List is saved in $SAVING/AppBackup/packages-AUR-${file_version}.txt"
echo
