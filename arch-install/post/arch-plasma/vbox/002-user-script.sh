#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Add lines to .bashrc
echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
echo 'export MAKEFLAGS="-j3 -l2"' >> ~/.bashrc

# Use all cores for make and compress
sudo ./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# Uncomment the below line for VirtualBox
sudo pacman -S mesa virtualbox-guest-utils --noconfirm --needed

# Call common script
sh ../003-common-script.sh
