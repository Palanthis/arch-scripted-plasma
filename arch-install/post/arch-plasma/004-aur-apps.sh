#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Plasma from AUR
yaourt -S --needed --noconfirm pamac-tray-appindicator

# Virtualbox Extensions
yaourt -S --noconfirm --needed virtualbox-ext-oracle 

# Apps from AUR
yaourt -S --noconfirm --needed chromium-widevine
yaourt -S --noconfirm --needed gitkraken
yaourt -S --noconfirm --needed caffeine-ng
yaourt -S --noconfirm --needed megasync
yaourt -S --noconfirm --needed rar
yaourt -S --noconfirm --needed etcher
