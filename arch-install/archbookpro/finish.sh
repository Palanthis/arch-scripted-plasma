#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

rm -rf /mnt/root/pacman.conf
rm -rf /mnt/root/sudoers
rm -rf /mnt/root/superscript-post.sh
rm -rf /mnt/root/superscript-post-uefi.sh
rm -rf /mnt/root/.bashrc

umount -Rf /mnt

reboot
